import { Unit, UnitName } from '../DTO/Unit';

export class ProductDTO {
    Id: number;
    Name: string;
    Unit: Unit;
    Price: number;

    constructor(val?)
    {
        this.Id = val && val["id"];
        this.Name = val && val["name"];
        this.Price = val && val["price"];
        this.Unit = val && new Unit(val["unit"]);
        if(!val)
        {
            this.Unit = new Unit();
            this.Unit.UnitType = UnitName.sztuka;
        }
    }

    clone(): ProductDTO
    {
        let clone = new ProductDTO();
        clone.Id = this.Id;
        clone.Name = this.Name;
        clone.Unit = this.Unit;
        clone.Price = this.Price;

        return clone;
    }
}