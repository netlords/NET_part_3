export class RegisterDTO {
    UserRoles: string;
    Login: string;
    Password: string;
    ConfirmPassword: string;

    constructor(isSeller: boolean)
    {
        if(isSeller)
        {
            this.UserRoles = "Seller";
        }
        else
        {
            this.UserRoles = "Client";
        }
    }
}