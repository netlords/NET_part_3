export class ShopDTO {
    Id: number;
    Name: string;
    Address: string;
    Latitude: number;
    Longitude: number;

    constructor(val?)
    {
        this.Id = val && val["id"];
        this.Name = val && val["name"];
        this.Address = val && val["address"];
        this.Latitude = val && val["latitude"];
        this.Longitude = val && val["longitude"];

    }

    clone(): ShopDTO {
        let clone = new ShopDTO();
        clone.Id = this.Id;
        clone.Name = this.Name;
        clone.Address = this.Address;
        clone.Latitude = this.Latitude;
        clone.Longitude = this.Longitude;
        return clone;
    }
}