export class ReceiptProductDTO {
    Id: number;
    ReceiptId: number;
    ProductId: number;
    Quantity: number;

    constructor(val?)
    {
        this.Id = val && val["id"];
        this.ReceiptId = val && val["receiptId"];
        this.ProductId = val && val["productId"];
        this.Quantity = val && val["quantity"];
    }

    static generate(receiptId: number, productId: number): ReceiptProductDTO
    {
        let ret = new ReceiptProductDTO();
        ret.ProductId = productId;
        ret.ReceiptId = receiptId;
        ret.Quantity = 1;
        return ret;
    }
}