export enum UnitName {
    sztuka,
    kilogram,
    litr,
}

export class Unit {
    static Values() : Unit[] {
        let units = new Array<Unit>();
        units[0] = new Unit();
        units[0].UnitType = UnitName.sztuka;
        units[1] = new Unit();
        units[1].UnitType = UnitName.kilogram;
        units[2] = new Unit();
        units[2].UnitType = UnitName.litr;

        return units;
    }

    UnitType: UnitName;

    constructor(val?)
    {
        this.UnitType = val && val["unitType"];
    }

    Name() 
    {
        switch(this.UnitType)
        {
            case UnitName.sztuka:
                return "sztuka";
            case UnitName.litr:
                return "litr";
            case UnitName.kilogram:
                return "kilogram";
            default:
                return "";
        }
    }
}