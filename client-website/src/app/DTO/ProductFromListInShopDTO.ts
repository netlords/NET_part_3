import { ShopProductDTO } from '../DTO/ShopProductDTO';
import { ProductDTO } from '../DTO/ProductDTO';
import { ReceiptProductDTO } from '../DTO/ReceiptProductDTO';

export class ProductFromListInShopDTO {
    ShopProduct: ShopProductDTO;
    Product: ProductDTO;
    WantedQuantity: number;
    EnoughUnits: boolean;
    ReceiptProduct: ReceiptProductDTO;

    constructor(val)
    {
        this.ShopProduct = val["shopProduct"] && new ShopProductDTO(val["shopProduct"]);
        this.Product = new ProductDTO(val["product"]);
        this.WantedQuantity = val["wantedQuantity"];
        this.EnoughUnits = val["enoughUnits"];
        this.ReceiptProduct = new ReceiptProductDTO(val["receiptProduct"]);
    }
}