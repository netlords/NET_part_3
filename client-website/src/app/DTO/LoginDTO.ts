export class LoginDTO {
    Login: string;
    Password: string;
    RememberMe: boolean;

    constructor()
    {
        this.RememberMe = false;
    }
}