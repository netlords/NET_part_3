export class ReceiptDTO {
    Id: number;
    AuthorId: number;
    Name: string;
    Done: boolean;

    constructor(val?)
    {
        this.Id = val && val["id"];
        this.AuthorId = val && val["authorId"];
        this.Name = val && val["name"];
        this.Done = val && val["done"];
    }

    clone(): ReceiptDTO
    {
        let clone = new ReceiptDTO();
        clone.Id = this.Id;
        clone.AuthorId = this.AuthorId;
        clone.Name = this.Name;
        clone.Done = this.Done;

        return clone;
    }
}