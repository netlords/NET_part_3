export class SharedReceiptDTO {
    Id: number;
    ReceiptId: number;
    UserId: string;

    constructor(val?)
    {
        this.Id = val && val["id"];
        this.ReceiptId = val && val["receiptId"];
        this.UserId = val && val["userId"];

    }
}