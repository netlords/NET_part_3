import { ProductDTO } from '../DTO/ProductDTO';

export class ShopProductDTO {
    Id: number;
    ShopId: number;
    Product: ProductDTO;
    AvailableUnits: number;
    Aisle: string;

    constructor(val?)
    {
        this.Id = val && val["id"];
        this.ShopId = val && val["shopId"];
        this.Product = val && new ProductDTO( val["product"]);
        this.AvailableUnits = val && val["availableUnits"];
        this.Aisle = val && val["aisle"];

        if(!val)
        {
            this.Product = new ProductDTO();
        }
    }

    clone(): ShopProductDTO {
        let clone = new ShopProductDTO();
        clone.Id = this.Id;
        clone.ShopId = this.ShopId;
        clone.Product = this.Product.clone();
        clone.AvailableUnits = this.AvailableUnits;
        clone.Aisle = this.Aisle;
        return clone;
    }
}