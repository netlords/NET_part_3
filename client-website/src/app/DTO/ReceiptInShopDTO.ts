import { ShopDTO } from '../DTO/ShopDTO';

export class ReceiptInShopDTO {
    Shop: ShopDTO;
    Availability: number;

    constructor(val)
    {
        this.Shop = new ShopDTO(val["shop"]);
        this.Availability = val["availability"];
    }
}