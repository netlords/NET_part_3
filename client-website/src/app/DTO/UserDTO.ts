export class UserDTO {
    Id: string;
    Username: string;
    Roles: string[];

    constructor(val?)
    {
        this.Id = val && val["id"];
        this.Username = val && val["username"];
        this.Roles = val && val["roles"];

        if(!val)
            this.Roles = new Array<string>();
    }
}