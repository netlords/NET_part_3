import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LogInService } from '../../services/log-in.service';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-client-page-template',
  templateUrl: './client-page-template.component.html',
  styleUrls: ['./client-page-template.component.css']
})
export class ClientPageTemplateComponent implements OnInit {
  sitePart: string;
  userName: string = "";

  constructor(private route: ActivatedRoute, private loginService: LogInService, private userService: UserService) { }

  ngOnInit() {
    this.userName = this.userService.readCurrentUserCookie().Username;

    switch (this.route.snapshot.url[0].path) {
      case "receipts": {
        this.sitePart = "receipts";
        break;
      }
      case "receipt": {
        this.sitePart = "receipts";
        break;
      }
      case "sharedReceipts": {
        this.sitePart = "sharedReceipts";
        break;
      }
      case "sharedReceipt": {
        this.sitePart = "sharedReceipts";
        break;
      }
    }
  }

  logout() {
    this.loginService.logOut().subscribe(() => {
      this.userService.redirectToHomeForUserType();
    });
  }
}
