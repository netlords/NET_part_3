import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientPageTemplateComponent } from './client-page-template.component';

describe('ClientPageTemplateComponent', () => {
  let component: ClientPageTemplateComponent;
  let fixture: ComponentFixture<ClientPageTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientPageTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientPageTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
