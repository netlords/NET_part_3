import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { RegisterDTO } from '../../DTO/RegisterDTO';
import { SignUpService } from '../../services/sign-up.service';
import { Observable } from 'rxjs/Observable';
import { ErrorMessage } from '../../validators/error-message';
import { RegisterValidator } from '../../validators/register-validator';

@Component({
  selector: 'app-sign-up-form',
  templateUrl: './sign-up-form.component.html',
  styleUrls: ['./sign-up-form.component.css']
})
export class SignUpFormComponent implements OnInit {
  _isSeller: boolean;
  @Input() set isSeller(value) { this._isSeller = value == "true"; }
  @Output() signedUp = new EventEmitter<Observable<boolean>>();
  @Input() signupErrorMessage: ErrorMessage = new ErrorMessage();
  registerValidator: RegisterValidator;

  singupData: RegisterDTO;

  constructor(private singupService: SignUpService, private router: Router) {
    this.registerValidator = new RegisterValidator(this.singupService);
  }

  ngOnInit() {
    this.singupData = new RegisterDTO(this._isSeller);
  }

  signUp() {
    this.registerValidator.validate(this.singupData).subscribe(res => {
      if (!res)
        return;
      this.signedUp.emit(this.singupService.signUp(this.singupData));
    });
  }
}
