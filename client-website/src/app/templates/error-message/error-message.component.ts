import { Component, OnInit, Input } from '@angular/core';
import { ErrorMessage } from '../../validators/error-message';

@Component({
  selector: 'app-error-message',
  templateUrl: './error-message.component.html',
  styleUrls: ['./error-message.component.css']
})
export class ErrorMessageComponent implements OnInit {
  @Input() errorMessage: ErrorMessage;

  constructor() { }

  ngOnInit() {
  }

}
