import { Component, OnInit, Input } from '@angular/core';
import { ErrorMessage } from '../../validators/error-message';

@Component({
  selector: 'app-error-alert',
  templateUrl: './error-alert.component.html',
  styleUrls: ['./error-alert.component.css']
})
export class ErrorAlertComponent implements OnInit {
  @Input() errorMessage: ErrorMessage;

  constructor() { }

  ngOnInit() {
  }

}
