import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellerPageTemplateComponent } from './seller-page-template.component';

describe('SellerPageTemplateComponent', () => {
  let component: SellerPageTemplateComponent;
  let fixture: ComponentFixture<SellerPageTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellerPageTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellerPageTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
