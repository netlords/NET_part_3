import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../../services/user.service';
import { LogInService } from '../../services/log-in.service';

@Component({
  selector: 'app-seller-page-template',
  templateUrl: './seller-page-template.component.html',
  styleUrls: ['./seller-page-template.component.css']
})
export class SellerPageTemplateComponent implements OnInit {
  sitePart: string;
  userName: string = "";

  constructor(private route: ActivatedRoute, private userService: UserService, private loginService: LogInService) { }

  ngOnInit() {
    this.userName = this.userService.readCurrentUserCookie().Username;

    this.sitePart = null;
    if (this.route.snapshot.url[1]) {
      switch (this.route.snapshot.url[1].path) {
        case "shop": {
          this.sitePart = "shops";
          break;
        }
        case "shops": {
          this.sitePart = "shops";
          break;
        }
        case "products": {
          this.sitePart = "products";
          break;
        }
      }
    }
  }

  logout() {
    this.loginService.logOut().subscribe(() => {
      this.userService.redirectToHomeForUserType();
    });
  }
}
