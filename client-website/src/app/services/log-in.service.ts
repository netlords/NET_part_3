import { Injectable } from '@angular/core';

import { LoginDTO } from '../DTO/LoginDTO';

import { HelperService } from './helper.service';


import { Observable } from 'rxjs/observable';
import { of } from 'rxjs/observable/of';

import { catchError, map } from 'rxjs/operators';
import { UserService } from './user.service';


@Injectable()
export class LogInService {

  constructor( private helperService: HelperService, private userService: UserService) { }

  logIn(loginData: LoginDTO): Observable<boolean> {
    return this.helperService.standardPost<boolean>("Account/Login", loginData);
  }
  logOut(): Observable<boolean> {
    this.userService.forgetUser();
    return this.helperService.standardPost<boolean>("Account/Logout", null);
  }
}
