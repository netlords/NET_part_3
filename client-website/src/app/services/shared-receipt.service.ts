import { Injectable } from '@angular/core';
import { SharedReceiptDTO } from '../DTO/SharedReceiptDTO';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { ReceiptDTO } from '../DTO/ReceiptDTO';
import { HelperService } from './helper.service';
import { UserDTO } from '../DTO/UserDTO';
import { map } from 'rxjs/operators/map';

@Injectable()
export class SharedReceiptService {

  constructor(private helperService: HelperService ) { }

  addShareRecord(record: SharedReceiptDTO): Observable<any> {
    return this.helperService.standardPost("SharedReceipt", record, 'addSharedReceipt');

  }

  deleteShareRecord(record: SharedReceiptDTO): Observable<any> {
    return this.helperService.standardDelete("SharedReceipt/" + record.ReceiptId + "/" + record.UserId, 'deleteSharedReceipt');
  }

  getUsersWithAccessToReceipt(receipt: ReceiptDTO): Observable<UserDTO[]> {
    return this.helperService.standardGet("SharedReceipt/GetUsersWithAccessToReceipt/" + receipt.Id).pipe(
      map(res => {
        let users = new Array<UserDTO>();
        let i = 0;
        for (let val of res) {
          users[i] = new UserDTO(val);
          i++;
        }
        return users;
      })
    );
  }

  getSharedReceiptsForCurrentUser(): Observable<ReceiptDTO[]> {
    return this.helperService.standardGet("SharedReceipt/GetSharedForCurrentUser").pipe(
      map(res => {
        let sharedReceipts = new Array<ReceiptDTO>();
        let i = 0;
        for (let val of res) {
          sharedReceipts[i] = new ReceiptDTO(val);
          i++;
        }
        return sharedReceipts;
      })
    );
  }

  changeDoneState(receipt: ReceiptDTO): Observable<any> {
    return this.helperService.standardPut("Receipt/ChangeDone/"+receipt.Id, "changeDoneState");
  }
}
