import { Injectable } from '@angular/core';
import { HelperService } from './helper.service';
import { UserDTO } from '../DTO/UserDTO';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import * as Cookies from 'es-cookie';
import { Router, ActivatedRoute, ActivationEnd } from '@angular/router';


@Injectable()
export class UserService {

  constructor(private helperService: HelperService, private router: Router, private route: ActivatedRoute) { }

  redirectToHomeForUserType() {
    let user = this.readCurrentUserCookie();
    if (user === null) {
      this.router.navigate(['/']);
    }
    else if (user.Roles.includes("Seller")) {
      this.router.navigate(['/seller']);
    }
    else if (user.Roles.includes("Client")) {
      this.router.navigate(['/receipts']);
    }
    else {
      this.router.navigate(['/']);
    }
  }

  isRouteAccessibleForUser(routeActivationEnd: ActivationEnd): boolean {
    let user = this.readCurrentUserCookie();
    let url = routeActivationEnd.snapshot.url;
    if ((url.length === 0) && (user.Roles && (user.Roles.includes("Seller") || user.Roles.includes("Client"))))
      return false;
    if ((url.length > 0 && url[0].path === "seller") && (!user.Roles || !user.Roles.includes("Seller")))
      return false;
    if ((url.length > 0 && url[0].path != "seller" && url[0].path != "signup") && (!user.Roles || !user.Roles.includes("Client")))
      return false;
    return true;
  }

  redirectIfRouteNotAccessibleForUser(routeActivationEnd: ActivationEnd) {
    let cookie = Cookies.get("currentUser");
    if (!cookie) {
      this.saveCurrentUser().subscribe(() => this.redirectIfRouteNotAccessibleForUser(routeActivationEnd));
    }
    else {
      if (!this.isRouteAccessibleForUser(routeActivationEnd))
        this.redirectToHomeForUserType();
    }
  }

  getCurrentUser(): Observable<UserDTO> {
    return this.helperService.standardGet<UserDTO[]>("Account/GetCurrentUser", [401]).pipe(
      map(res => {
        return new UserDTO(res);
      })
    );
  }

  saveCurrentUser(): Observable<any> {
    return this.getCurrentUser().pipe(
      map(res => {
        Cookies.set("currentUser", JSON.stringify(res));
      })
    );
  }

  readCurrentUserCookie(): UserDTO {
    let cookie = Cookies.get("currentUser");
    if (!cookie) {
      return null;
    }
    else {
      return JSON.parse(cookie);
    }
  }

  forgetUser() {
    Cookies.remove("currentUser");
  }

  getAllUsers() {
    return this.helperService.standardGet<UserDTO[]>("Account/GetLogins").pipe(
      map(res => {
        let users = new Array<UserDTO>();
        let i = 0;
        for (let val of res) {
          users[i] = new UserDTO(val);
          i++;
        }
        return users;
      })
    );
  }
}
