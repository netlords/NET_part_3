import { Injectable } from '@angular/core';
import 'rxjs/Rx';

import { ReceiptDTO } from '../DTO/ReceiptDTO';
import { ReceiptProductDTO } from '../DTO/ReceiptProductDTO';
import { ReceiptInShopDTO } from '../DTO/ReceiptInShopDTO';
import { ShopDTO } from '../DTO/ShopDTO';
import { ShopProductDTO } from '../DTO/ShopProductDTO';
import { ProductDTO } from '../DTO/ProductDTO';
import { ProductFromListInShopDTO } from '../DTO/ProductFromListInShopDTO';

import { HelperService } from './helper.service';
import { ProductService } from './product.service';

import { Observable } from 'rxjs/observable';
import { of } from 'rxjs/observable/of';

import { catchError, map } from 'rxjs/operators';


@Injectable()
export class ReceiptService {

  constructor(private helperService: HelperService, private productService: ProductService) {

  }

  getReceipts(): Observable<ReceiptDTO[]> {
    return this.helperService.standardGet<ReceiptDTO[]>("Receipt").pipe(
      map(res => {
        let receipts = new Array<ReceiptDTO>();
        let i = 0;
        for (let val of res) {
          receipts[i] = new ReceiptDTO(val);
          i++;
        }
        return receipts;
      })
    );
  }

  getReceipt(id: number): Observable<ReceiptDTO> {
    return this.helperService.standardGet<ReceiptDTO>("Receipt/" + id).pipe(
      map(res => {
        if(!res)
          return null;
        return new ReceiptDTO(res);
      })
    );
  }

  getReceiptInShops(receiptId: number): Observable<ReceiptInShopDTO[]> {
    return this.helperService.standardGet<ReceiptInShopDTO[]>("Receipt/ReceiptInShops/"+receiptId).pipe(
      map(res => {
        let receiptInShops = new Array<ReceiptInShopDTO>();
        let i=0;
        if(res)
        {
          for(let val of res)
          {
            receiptInShops[i] = new ReceiptInShopDTO(val);
            i++;
          }
        }
        return receiptInShops;
      })
    );
  }

  GetProductsFromListInShop(receiptId: number, shopId: number): Observable<ProductFromListInShopDTO[]> {
    return this.helperService.standardGet<ProductFromListInShopDTO[]>
    ("Receipt/ProductsFromListInShop/" + receiptId + "/" + shopId).pipe(
      map(res => {
        let products = new Array<ProductFromListInShopDTO>();
        let i = 0;
        for (let val of res) {
          products[i] = new ProductFromListInShopDTO(val);
          i++;
        }
        return products;
      })
    );
  }

  addReceipt(receipt: ReceiptDTO): Observable<any> {
    return this.helperService.standardPost("Receipt", receipt, 'addReceipt');
  }

  updateReceipt(receipt: ReceiptDTO): Observable<any> {
    return this.helperService.standardPut("Receipt/" + receipt.Id, receipt, 'updateReceipt');
  }

  deleteReceipt(receipt: ReceiptDTO): Observable<any> {
    return this.helperService.standardDelete("Receipt/" + receipt.Id, 'deleteReceipt');
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
    //  if (result.status === 401) console.error("YAY");
      //else console.error("NAY");
      // TODO: send the error to remote logging infrastructure
     // console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
