import { Injectable } from '@angular/core';
import { RegisterDTO } from '../DTO/RegisterDTO';

import { HelperService } from './helper.service';


import { Observable } from 'rxjs/observable';
import { of } from 'rxjs/observable/of';

import { catchError, map } from 'rxjs/operators';
import { StringDTO } from '../DTO/StringDTO';
@Injectable()
export class SignUpService {

  constructor(private helperService: HelperService) { }

  signUp(registerData: RegisterDTO): Observable<boolean> {
    // httpOptions
    if (registerData.UserRoles === 'Client')
      return this.helperService.standardPost<boolean>("Account/Register", registerData);
    else
      return this.helperService.standardPost<boolean>("Account/RegisterSeller", registerData);
  }

  checkIfUserNameAvailable(login: string): Observable<boolean>{
    return this.helperService.standardPost<boolean>("Account/CheckIfUserNameAvailable", new StringDTO(login));
  }
}
