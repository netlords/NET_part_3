import { Injectable } from '@angular/core';

import { ShopProductDTO } from '../DTO/ShopProductDTO';
import { ShopDTO } from '../DTO/ShopDTO';
import { ProductFromListInShopDTO } from '../DTO/ProductFromListInShopDTO';

import { HelperService } from './helper.service';
import { ProductService } from './product.service';

import { Observable } from 'rxjs/observable';
import { of } from 'rxjs/observable/of';

import { catchError, map } from 'rxjs/operators';
@Injectable()
export class ShopProductService {

  constructor(private helperService: HelperService) {

  }

  getShopProducts(): Observable<ShopProductDTO[]> {
    return this.helperService.standardGet<ShopProductDTO[]>("ShopProduct").pipe(
      map(res => {
        let shopProducts = new Array<ShopProductDTO>();
        let i = 0;
        for (let val of res) {
          shopProducts[i] = new ShopProductDTO(val);
          i++;
        }
        return shopProducts;
      })
    );
  }

  getShopProductsByShopId(shopId: number): Observable<ShopProductDTO[]>{
    return this.helperService.standardGet<ShopProductDTO[]>("ShopProduct/Shop/" + shopId).pipe(
      map(res => {
        let shopProducts = new Array<ShopProductDTO>();
        let i = 0;
        for (let val of res) {
          shopProducts[i] = new ShopProductDTO(val);
          i++;
        }
        return shopProducts;
      })
    );
  }
  getShopProduct(id: number): Observable<ShopProductDTO> {
    return this.helperService.standardGet<ShopProductDTO>("ShopProduct/" + id).pipe(
      map(res => {
        return new ShopProductDTO(res);
      })
    );
  }

  getDefaultProduct(): Observable<ShopProductDTO> {
    return this.getShopProduct(-1);
  }

  addShopProduct(shopProduct: ShopProductDTO): Observable<any> {
    return this.helperService.standardPost("ShopProduct", shopProduct,'addShopProduct');
  }

  updateShopProduct(shopProduct: ShopProductDTO): Observable<any> {
    return this.helperService.standardPut("ShopProduct/" + shopProduct.Id, shopProduct, 'updateShopProduct');
  }

  deleteShopProduct(shopProduct: ShopProductDTO): Observable<any> {
    return this.helperService.standardDelete("ShopProduct/" + shopProduct.Id, 'deleteShopProduct');
  }

}
