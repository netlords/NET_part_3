import { Router } from "@angular/router";
import 'rxjs/Rx';
import 'rxjs/add/operator/catch';
import { catchError } from 'rxjs/operators/catchError';
import { map } from 'rxjs/operators/map';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/observable';
import { RouterModule, Routes } from '@angular/router';
import { of } from 'rxjs/observable/of';

import { environment } from '../../../src/environments/environment';
import { Response } from '@angular/http/src/static_response';
@Injectable()
export class HelperService {
  headers: HttpHeaders;

  constructor(private http: HttpClient, private router: Router) {
    this.headers = new HttpHeaders();
    this.headers.append('Content-Type', 'application/json');
  }

  standardGet<T>(url: string, ignore: number[] = null, errorMessage?: string): Observable<any> {
    return this.http.get<T>(environment.apiUrl + url, { withCredentials: true, headers: this.headers }).pipe(
      catchError(this.handleError<any>(errorMessage, ignore)));
  }
  standardPost<T>(url: string, requestBody: any, errorMessage?: string): Observable<any> {
    return this.http.post(environment.apiUrl + url, requestBody, { withCredentials: true, headers: this.headers }).pipe(
      catchError(this.handleError<any>(errorMessage)));
  }
  standardPut<T>(url: string, requestBody: any, errorMessage?: string): Observable<any> {
    return this.http.put(environment.apiUrl + url, requestBody, { withCredentials: true, headers: this.headers }).pipe(
      catchError(this.handleError<any>(errorMessage)));

  }
  standardDelete<T>(url: string, errorMessage?: string): Observable<any> {
    return this.http.delete(environment.apiUrl + url, { withCredentials: true, headers: this.headers }).pipe(
      catchError(this.handleError<any>(errorMessage)));
  }
  private handleError<T>(operation = 'operation', ignore: number[] = null, result?: T) {
    return (error: any): Observable<T> => {
      if (error.status === 401) {
        this.router.navigate(['/']);
      }
      // TODO: send the error to remote logging infrastructure
      if (!(ignore && ignore.includes(error.status)))
        console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
