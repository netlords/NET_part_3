import { Injectable } from '@angular/core';
import { catchError, map } from 'rxjs/operators';
import { Observable } from 'rxjs/observable';
import { of } from 'rxjs/observable/of';

import { ProductDTO } from '../DTO/ProductDTO';

import { HelperService } from './helper.service';


import { Response } from '@angular/http/src/static_response';


@Injectable()
export class ProductService {
  constructor(private helperService: HelperService) {
  }
  getProduct(id: number): Observable<ProductDTO> {

    return this.helperService.standardGet<ProductDTO>("Product/" + id).pipe(
      map(res => {
        return new ProductDTO(res);
      })
    );
  }



  getProducts(): Observable<ProductDTO[]> {
    return this.helperService.standardGet<ProductDTO[]>("Product").pipe(
      map(res => {
        let products = new Array<ProductDTO>();
        let i = 0;
        for (let val of res) {
          products[i] = new ProductDTO(val);
          i++;
        }
        return products;
      })
    );
  }
  addProduct(product: ProductDTO): Observable<any> {
    return this.helperService.standardPost("Product", product, 'addProduct');
  }

  updateProduct(product: ProductDTO): Observable<any> {
    return this.helperService.standardPut("Product/" + product.Id, product, 'updateProduct');
  }

  deleteProduct(product: ProductDTO): Observable<any> {
    return this.helperService.standardDelete("Product/" + product.Id, 'deleteProduct');
  }

  productNameAlreadyExists(value: string, validName: string): Observable<boolean> {
    return this.getProducts().pipe(
      map(
        val => {
          if(validName && value === validName)
            return false;
          else
            return val.some(s => s.Name == value)
        }
      )
    );
  }

  getDefaultProduct(): Observable<ProductDTO> {
    return this.getProduct(-1);
  }

}
