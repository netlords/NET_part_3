import { TestBed, inject } from '@angular/core/testing';

import { SharedReceiptService } from './shared-receipt.service';

describe('SharedReceiptService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SharedReceiptService]
    });
  });

  it('should be created', inject([SharedReceiptService], (service: SharedReceiptService) => {
    expect(service).toBeTruthy();
  }));
});
