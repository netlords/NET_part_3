import { Injectable } from '@angular/core';
import { catchError, map } from 'rxjs/operators';
import { Observable } from 'rxjs/observable';
import { of } from 'rxjs/observable/of';

import { ShopDTO } from '../DTO/ShopDTO';

import { HelperService } from './helper.service';
import { ProductService } from './product.service';
import { ShopProductService } from './shop-product.service';
import { ReceiptProductService } from './receipt-product.service';

import { Response } from '@angular/http/src/static_response';
import { ProductDTO } from '../DTO/ProductDTO';
import { ShopProductDTO } from '../DTO/ShopProductDTO';


@Injectable()
export class ShopService {

  constructor(private helperService: HelperService, private productService: ProductService,
    private shopProductService: ShopProductService, private receiptProductService: ReceiptProductService) {
  }
  getShop(id: number): Observable<ShopDTO> {

    return this.helperService.standardGet<ShopDTO>("Shop/" + id).pipe(
      map(res => {
        return new ShopDTO(res);
      })
    );
  }



  getShops(): Observable<ShopDTO[]> {
    return this.helperService.standardGet<ShopDTO[]>("Shop").pipe(
      map(res => {
        let shops = new Array<ShopDTO>();
        let i = 0;
        for (let val of res) {
          shops[i] = new ShopDTO(val);
          i++;
        }
        return shops;
      })
    );
  }
  addShop(shop: ShopDTO): Observable<any> {
    return this.helperService.standardPost("Shop", shop, 'addShop');
  }

  updateShop(shop: ShopDTO): Observable<any> {
    return this.helperService.standardPut("Shop/" + shop.Id, shop, 'updateShop');
  }

  deleteShop(shop: ShopDTO): Observable<any> {
    return this.helperService.standardDelete("Shop/" + shop.Id, 'deleteShop');
  }


  shopNameAlreadyExists(value: string, validName: string): Observable<boolean> {
    return this.getShops().pipe(
      map(
        val => {
          if(validName && value === validName)
            return false;
          else
            return val.some(s => s.Name == value)
        }
      )
    );
  }

  getDefaultShop(): Observable<ShopDTO> {
    return this.getShop(-1);
  }

  GetFilteredProductsNotInShop(shop: ShopDTO, filterString: string): ProductDTO[] {
    let products = new Array<ProductDTO>();
    this.productService.getProducts().subscribe(val => products = val);
    let shopProducts = new Array<ShopProductDTO>();
    this.shopProductService.getShopProductsByShopId(shop.Id).subscribe(val => shopProducts = val);


    if (filterString != "" && filterString != null) {
      products = products.filter(p => p.Name.toLowerCase().includes(filterString.toLowerCase()));
    }

    products = products.filter(p =>
      shopProducts
        .filter(sp => sp.Product.Id == p.Id && sp.ShopId == shop.Id).length == 0);

    return products.sort((a , b) => a.Name.localeCompare(b.Name));
  }



}
