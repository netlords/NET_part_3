import { Injectable } from '@angular/core';
import { catchError, map } from 'rxjs/operators';
import { Observable } from 'rxjs/observable';
import { of } from 'rxjs/observable/of';

import { HelperService } from './helper.service';
import { ProductService } from './product.service';

import { ReceiptProductDTO } from '../DTO/ReceiptProductDTO';
import { ProductFromListInShopDTO } from '../DTO/ProductFromListInShopDTO';
import { ProductDTO } from '../DTO/ProductDTO';


@Injectable()
export class ReceiptProductService {

  constructor(private helperService: HelperService, private productService: ProductService) { }

  addProductToReceipt(receiptProduct: ReceiptProductDTO): Observable<any> {
    return this.helperService.standardPost( "ReceiptProduct", receiptProduct, 'addReceiptProduct');
  }

  updateProductOnReceipt(receiptProduct: ReceiptProductDTO): Observable<any> {
    return this.helperService.standardPut("ReceiptProduct/" + receiptProduct.Id, receiptProduct, 'updateReceiptProduct');
  }

  deleteProductFromReceipt(product: ReceiptProductDTO): Observable<any> {
    return this.helperService.standardDelete("ReceiptProduct/" + product.Id, 'deleteReceiptProduct');
  }

  getReceiptProductsFromReceipt(receiptId: number): Observable<ReceiptProductDTO[]> {
    return this.helperService.standardGet<ReceiptProductDTO[]>("ReceiptProduct/" + receiptId).pipe(
      map(res => {
        let receipts = new Array<ReceiptProductDTO>();
        let i = 0;
        for (let val of res) {
          receipts[i] = new ReceiptProductDTO(val);
          i++;
        }
        return receipts;
      })
    );
  }
  getTotalReceiptPrice(receiptId:number)
  {
    let totalPrice = 0;

    let receiptProducts = new Array<ReceiptProductDTO>();
    this.getReceiptProductsFromReceipt(receiptId).subscribe(val => receiptProducts = val);
    for (let p of receiptProducts)
    {
        let product = new ProductDTO();
        this.productService.getProduct(p.ProductId).subscribe(val => product = val);
        if (product != null)
        {
            totalPrice += product.Price * p.Quantity;
        }
    }
    return totalPrice;
  }

}
