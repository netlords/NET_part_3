import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }    from '@angular/forms';


import { HttpClientModule, HttpClient } from '@angular/common/http';
import { AppComponent } from './app.component';

import { AppRoutingModule } from './/app-routing.module';

import { HelperService } from './services/helper.service';
import { ReceiptService } from './services/receipt.service';
import { ProductService } from './services/product.service';
import { ShopProductService } from './services/shop-product.service';
import { ReceiptProductService } from './services/receipt-product.service';
import { LogInService } from './services/log-in.service';
import { SignUpService } from './services/sign-up.service';
import { UserService } from './services/user.service';
import { ShopService } from './services/shop.service';

import { LoginComponent } from './home-website/login/login.component';
import { SignupComponent } from './home-website/signup/signup.component';
import { ClientStartPageComponent } from './home-website/client-start-page/client-start-page.component';
import { ReceiptsComponent } from './client-website/receipts/receipts.component';
import { ReceiptsListComponent } from './client-website/receipts-list/receipts-list.component';
import { ShopsMapWindowComponent } from './client-website/shops-map-window/shops-map-window.component';
import { SellerStartPageComponent } from './seller-website/seller-start-page/seller-start-page.component';
import { ProductsListComponent } from './seller-website/products-list/products-list.component';
import { ShopsListComponent } from './seller-website/shops-list/shops-list.component';
import { ShopMapWindowComponent } from './seller-website/shop-map-window/shop-map-window.component';
import { ShopComponent } from './seller-website/shop/shop.component';
import { PageTitleTemplateComponent } from './templates/page-title-template/page-title-template.component';
import { SellerPageTemplateComponent } from './templates/seller-page-template/seller-page-template.component';
import { ClientPageTemplateComponent } from './templates/client-page-template/client-page-template.component';
import { ShareDialogComponent } from './client-website/share-dialog/share-dialog.component';
import { SharedReceiptService } from './services/shared-receipt.service';
import { SharedReceiptsListComponent } from './client-website/shared-receipts-list/shared-receipts-list.component';
import { SharedReceiptComponent } from './client-website/shared-receipt/shared-receipt.component';
import { RegisterComponent } from './seller-website/register/register.component';
import { SignUpFormComponent } from './templates/sign-up-form/sign-up-form.component';
import { ErrorMessageComponent } from './templates/error-message/error-message.component';
import { ErrorAlertComponent } from './templates/error-alert/error-alert.component';

@NgModule({
  declarations: [
    AppComponent,
    ClientStartPageComponent,
    ReceiptsComponent,
    ReceiptsListComponent,
    ShopsMapWindowComponent,
    LoginComponent,
    SignupComponent,
    SellerStartPageComponent,
    ProductsListComponent,
    ShopsListComponent,
    ShopMapWindowComponent,
    ShopComponent,
    PageTitleTemplateComponent,
    SellerPageTemplateComponent,
    ClientPageTemplateComponent,
    ShareDialogComponent,
    SharedReceiptsListComponent,
    SharedReceiptComponent,
    RegisterComponent,
    SignUpFormComponent,
    ErrorMessageComponent,
    ErrorAlertComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule, 
    HttpClientModule,
    FormsModule
  ],
  providers: [ReceiptService, HelperService, ProductService, ShopProductService, 
    ReceiptProductService, LogInService, SignUpService, ShopService, UserService,
     SharedReceiptService],
  bootstrap: [AppComponent]
})
export class AppModule { }
