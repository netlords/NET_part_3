import { Component, OnInit } from '@angular/core';
import { UserService } from './services/user.service';
import { Router, NavigationEnd, ActivationEnd } from '@angular/router';

declare function initMaterialInputs();

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';

  constructor(private userService: UserService, private router: Router) {}

  ngOnInit() {
    this.router.events.subscribe(val => {
      if(val instanceof ActivationEnd)
      {
        this.userService.redirectIfRouteNotAccessibleForUser(val);
      }
    });
  }
}
