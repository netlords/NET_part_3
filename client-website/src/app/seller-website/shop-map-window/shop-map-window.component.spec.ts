import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopMapWindowComponent } from './shop-map-window.component';

describe('ShopMapWindowComponent', () => {
  let component: ShopMapWindowComponent;
  let fixture: ComponentFixture<ShopMapWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShopMapWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopMapWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
