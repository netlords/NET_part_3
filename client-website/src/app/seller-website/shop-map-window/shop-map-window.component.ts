import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ShopDTO } from '../../DTO/ShopDTO';

declare function setShop(shop: ShopDTO): any;
declare function setComponent(component: ShopMapWindowComponent): any;

@Component({
  selector: 'app-shop-map-window',
  templateUrl: './shop-map-window.component.html',
  styleUrls: ['./shop-map-window.component.css']
})
export class ShopMapWindowComponent implements OnInit {
  @Input() shop: ShopDTO;
  @Output() shopChange = new EventEmitter<ShopDTO>();

  constructor() { }

  ngOnInit() {
    setComponent(this);
  }

  setShopLocation(latitude: number, longitude: number) {
    this.shop.Latitude = latitude;
    this.shop.Longitude = longitude;
    this.shopChange.emit(this.shop);
  }

  setShopInMap() {
    setShop(this.shop);
  }
}
