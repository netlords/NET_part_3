import { Component, OnInit } from '@angular/core';
import { ShopProductDTO } from '../../DTO/ShopProductDTO';
import { ShopProductService } from '../../services/shop-product.service';
import { ActivatedRoute } from '@angular/router';
import { ProductDTO } from '../../DTO/ProductDTO';
import { ProductService } from '../../services/product.service';
import { ShopDTO } from '../../DTO/ShopDTO';
import { ShopService } from '../../services/shop.service';

import { ErrorMessage } from '../../validators/error-message';
import { ShopProductValidator } from '../../validators/shop-product-validator';

declare function initMaterialInputs(); 
declare function closeModal(modalid: string);

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.css']
})
export class ShopComponent implements OnInit {
  filterText: string = "";
  products: ProductDTO[];
  filteredProducts: ProductDTO[];
  productsInShop: ShopProductDTO[];
  shop: ShopDTO = new ShopDTO();

  selectedProductInShop: ShopProductDTO = new ShopProductDTO();
  isSelectedNew: boolean = true;

  shopProductValidator: ShopProductValidator = new ShopProductValidator();

  constructor(
    private shopProductService: ShopProductService,
    private productService: ProductService,
    private shopService: ShopService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.getProductsInShop();
    this.getProducts();
    this.getShop();
    initMaterialInputs();
  }

  productsInShopChanged() {
    this.getProductsInShop();
  }

  getShop(){
    const id = +this.route.snapshot.paramMap.get('id');
    this.shopService.getShop(id).subscribe(res => this.shop = res);
  }

  getProductsInShop() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.shopProductService.getShopProductsByShopId(id).subscribe( res => {
      this.productsInShop = res;
      this.getFilteredProductsNotInShop();      
    });
  }

  getProducts(): void {
    this.productService.getProducts().subscribe(res => {
      this.products = res;
      this.getFilteredProductsNotInShop();
    });
  }

  getFilteredProductsNotInShop(): void {
    if(!this.products || !this.productsInShop)
      return;
    if(this.filterText === "")
    {
      this.filteredProducts = this.products;
    }
    else
    {
      this.filteredProducts = this.products.filter(product => product.Name.toLocaleLowerCase().includes(this.filterText.toLocaleLowerCase()));
    }
    
    this.filteredProducts = this.filteredProducts.filter(product => !this.productsInShop.some(productInShop => productInShop.Product.Id == product.Id));
  }

  deleteProductFromShop(productInShop: ShopProductDTO) {
    this.shopProductService.deleteShopProduct(productInShop).subscribe(() => {
      this.productsInShopChanged();
    })
  }

  onEditProductInShop(productInShop: ShopProductDTO) {
    this.selectedProductInShop = productInShop.clone();
    this.isSelectedNew = false;
    this.shopProductValidator.clear();
  }

  onAddProductToShop(product: ProductDTO) {
    const id = +this.route.snapshot.paramMap.get('id');
    this.selectedProductInShop = new ShopProductDTO();
    this.selectedProductInShop.Product = product;
    this.selectedProductInShop.ShopId = id;
    this.isSelectedNew = true;
    this.shopProductValidator.clear();
  }

  productModalSave() {
    if(!this.shopProductValidator.validate(this.selectedProductInShop))
      return;
    if(this.isSelectedNew)
    {
      this.shopProductService.addShopProduct(this.selectedProductInShop).subscribe(() => {
        this.productsInShopChanged();
      })
    }
    else
    {
      this.shopProductService.updateShopProduct(this.selectedProductInShop).subscribe(() => {
        this.productsInShopChanged();
      });
    }
    closeModal("#productModal");
  }
}
