import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellerStartPageComponent } from './seller-start-page.component';

describe('SellerStartPageComponent', () => {
  let component: SellerStartPageComponent;
  let fixture: ComponentFixture<SellerStartPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellerStartPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellerStartPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
