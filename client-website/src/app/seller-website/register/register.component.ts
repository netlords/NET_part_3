import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ErrorMessage } from '../../validators/error-message';

declare function showSnackbar(text: string);

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  signupErrorMessage: ErrorMessage = new ErrorMessage();

  constructor(private router: Router) { }

  ngOnInit() {
  }

  signUp(event: any)
  {
    event.subscribe(res => {
      if(res)
      {
        showSnackbar("Seller account created.");
        this.router.navigate(['/']);
      }
      else
      {
        this.signupErrorMessage.show("Sorry, sign up failed.");
      }
    })
  }
}
