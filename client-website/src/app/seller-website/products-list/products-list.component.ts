import { Component, OnInit } from '@angular/core';
import { ProductDTO } from '../../DTO/ProductDTO';
import { ProductService } from '../../services/product.service';
import { Unit, UnitName } from '../../DTO/Unit';
import { ErrorMessage } from '../../validators/error-message';
import { ProductValidator } from '../../validators/product-validator';

declare function initMaterialInputs();
declare function closeModal(modalid: string);

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.css']
})
export class ProductsListComponent implements OnInit {

  products: ProductDTO[];
  selectedProduct: ProductDTO = new ProductDTO();
  selectedProductName: string = null;
  isSelectedNew: boolean = true;
  unitValues: Unit[];

  productValidator: ProductValidator = new ProductValidator(this.productService);

  constructor(private productService: ProductService) { }

  ngOnInit() {
    this.getProducts();
    this.unitValues = Unit.Values();
    initMaterialInputs();
  }

  getProducts(): void {
    this.productService.getProducts().subscribe(res => this.products = res);
  }

  deleteProduct(product: ProductDTO): void {
    this.productService.deleteProduct(product).subscribe(() => this.getProducts());
  }

  onAddProduct(): void {
    this.selectedProduct = new ProductDTO();
    this.selectedProductName = null;
    this.isSelectedNew = true;
    this.productValidator.clear();
  }

  onEditProduct(product: ProductDTO): void {
    this.selectedProduct = product.clone();
    this.selectedProductName = product.Name;
    this.isSelectedNew = false;
    this.productValidator.clear();
  }

  productModalSave(): void {
    this.productValidator.validate(this.selectedProduct, this.selectedProductName).subscribe(res => {
      if (!res)
        return;
      if (this.isSelectedNew) {
        this.productService.addProduct(this.selectedProduct).subscribe(() => this.getProducts());
      }
      else {
        this.productService.updateProduct(this.selectedProduct).subscribe(() => this.getProducts());
      }
      closeModal("#productModal");
    });
  }


}
