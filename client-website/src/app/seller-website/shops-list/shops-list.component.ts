import { Component, OnInit } from '@angular/core';
import { ShopDTO } from '../../DTO/ShopDTO';
import { ShopService } from '../../services/shop.service';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { ErrorMessage } from '../../validators/error-message';
import { ShopValidator } from '../../validators/shop-validator';

declare function addSellerModalListener(): any;
declare function destroyModalListener(modalId: string, client: boolean): any;
declare function initMaterialInputs();
declare function closeModal(modalid: string);

@Component({
  selector: 'app-shops-list',
  templateUrl: './shops-list.component.html',
  styleUrls: ['./shops-list.component.css']
})
export class ShopsListComponent implements OnInit, OnDestroy {
  shops: ShopDTO[];
  selectedShop: ShopDTO = new ShopDTO();
  selectedShopName: string = null;
  isSelectedNew: boolean = true;

  shopValidator: ShopValidator = new ShopValidator(this.shopService);

  constructor(private shopService: ShopService) { }

  ngOnInit() {
    addSellerModalListener();
    this.getShops();
    initMaterialInputs();
  }

  ngOnDestroy() {
    destroyModalListener("#shopModal", false);
  }

  getShops(): void {
    this.shopService.getShops().subscribe(res => this.shops = res);
  }

  deleteShop(shop: ShopDTO): void {
    this.shopService.deleteShop(shop).subscribe(() => this.getShops());
  }

  onAddShop() {
    this.selectedShop = new ShopDTO();
    this.selectedShopName = null;
    this.isSelectedNew = true;
    this.shopValidator.clear();
  }

  onEditShop(shop: ShopDTO) {
    this.selectedShop = shop.clone();
    this.selectedShopName = shop.Name;
    this.isSelectedNew = false;
    this.shopValidator.clear();
  }

  shopModalSave(): void {
    this.shopValidator.validate(this.selectedShop, this.selectedShopName).subscribe(res => {
      if (!res)
        return;
      if (this.isSelectedNew) {
        this.shopService.addShop(this.selectedShop).subscribe(() => this.getShops());
      }
      else {
        this.shopService.updateShop(this.selectedShop).subscribe(() => this.getShops());
      }
      closeModal("#shopModal");
    });
  }
}
