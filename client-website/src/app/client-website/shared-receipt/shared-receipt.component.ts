import { Component, OnInit, ViewChild } from '@angular/core';
import { ReceiptDTO } from '../../DTO/ReceiptDTO';
import { ProductFromListInShopDTO } from '../../DTO/ProductFromListInShopDTO';
import { ShopDTO } from '../../DTO/ShopDTO';
import { ReceiptInShopDTO } from '../../DTO/ReceiptInShopDTO';
import { ShopsMapWindowComponent } from '../shops-map-window/shops-map-window.component';
import { ReceiptService } from '../../services/receipt.service';
import { ActivatedRoute, Router } from '@angular/router';

declare function addModalListener(modalId: string): any;
declare function createPdf(receipt: ReceiptDTO, products: ProductFromListInShopDTO[], shop: ShopDTO, totalPrice: number, print: boolean): any;

@Component({
  selector: 'app-shared-receipt',
  templateUrl: './shared-receipt.component.html',
  styleUrls: ['./shared-receipt.component.css']
})
export class SharedReceiptComponent implements OnInit {
  @ViewChild(ShopsMapWindowComponent) mapComponent: ShopsMapWindowComponent;

  receipt: ReceiptDTO = new ReceiptDTO();
  wantedProducts: ProductFromListInShopDTO[];
  selectedShop: ShopDTO;
  receiptInShops: ReceiptInShopDTO[];
  totalPrice: number;

  constructor(
    private receiptService: ReceiptService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    addModalListener("#sharedMapModal"); //in client-map.js
    this.getReceipt();
  }

  getReceipt(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.receiptService.getReceipt(id).subscribe(val => {
      if(!val)
      {
        this.router.navigate(['/sharedReceipts']);
        return;
      }
      this.receipt = val;
      this.getProductsFromListInShop();
      this.getReceiptInShops();
    });
  }

  getProductsFromListInShop(): void {
    this.receiptService.GetProductsFromListInShop(this.receipt.Id, this.selectedShop ? this.selectedShop.Id : -1).subscribe(val => {
      this.wantedProducts = val;
      this.calculateTotalPrice();
    });
  }

  getReceiptInShops(): void {
    this.receiptService.getReceiptInShops(this.receipt.Id).subscribe(res => {
      this.receiptInShops = res;
      this.mapComponent.setReceiptInShopsForMap(res);
    });
  }

  calculateTotalPrice(): void {
    let total: number = 0;
    for(let product of this.wantedProducts)
    {
      total += product.WantedQuantity * product.Product.Price;
    }
    this.totalPrice = total;
  }

  selectedShopChanged(): void {
    this.getProductsFromListInShop();
  }

  shopSelected(): void {
    this.mapComponent.selectedShopConfirm();
  }

  createPdf(print: boolean): void {
    createPdf(this.receipt, this.wantedProducts, this.selectedShop, this.totalPrice, print)
  }
}
