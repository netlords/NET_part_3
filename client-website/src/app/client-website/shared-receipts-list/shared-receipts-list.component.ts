import { Component, OnInit } from '@angular/core';
import { SharedReceiptService } from '../../services/shared-receipt.service';
import { ReceiptService } from '../../services/receipt.service';
import { ReceiptDTO } from '../../DTO/ReceiptDTO';

@Component({
  selector: 'app-shared-receipts-list',
  templateUrl: './shared-receipts-list.component.html',
  styleUrls: ['./shared-receipts-list.component.css']
})
export class SharedReceiptsListComponent implements OnInit {
  receipts: ReceiptDTO[];

  constructor(private sharedReceiptService: SharedReceiptService) { }

  ngOnInit() {
    this.getReceipts();
  }

  getReceipts(): void {
    this.sharedReceiptService.getSharedReceiptsForCurrentUser().subscribe(val => {this.receipts = val});
  }

  onChangeDoneState(receipt: ReceiptDTO)
  {
    receipt.Done = ! receipt.Done;
    this.sharedReceiptService.changeDoneState(receipt).subscribe();
  }
}
