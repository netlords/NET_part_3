import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SharedReceiptsListComponent } from './shared-receipts-list.component';

describe('SharedReceiptsListComponent', () => {
  let component: SharedReceiptsListComponent;
  let fixture: ComponentFixture<SharedReceiptsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SharedReceiptsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SharedReceiptsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
