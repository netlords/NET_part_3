import { Component, OnInit, Input, Output, EventEmitter, NgZone } from '@angular/core';

import { ReceiptInShopDTO } from '../../DTO/ReceiptInShopDTO';
import { ShopDTO } from '../../DTO/ShopDTO';

declare function setReceiptInShops(receiptInShops: ReceiptInShopDTO[], component: ShopsMapWindowComponent): any;

@Component({
  selector: 'app-shops-map-window',
  templateUrl: './shops-map-window.component.html',
  styleUrls: ['./shops-map-window.component.css']
})
export class ShopsMapWindowComponent implements OnInit {
  @Input() receiptInShops: ReceiptInShopDTO[];
  @Input() selectedShop: ShopDTO;
  @Output() selectedShopChange = new EventEmitter<ShopDTO>();

  selectedShopLocal: ShopDTO;

  constructor(private zone: NgZone) { 
  }

  ngOnInit() {
    this.updateSelectedShopLocalSelected();
  }

  updateSelectedShopLocalSelected() {
    this.zone.run(() => { 
      this.selectedShopLocal = this.selectedShop;
    });
  }

  selectShop(shopId: number)
  {
    this.zone.run(() => { 
      this.selectedShopLocal = this.receiptInShops.find(ris => ris.Shop.Id === shopId).Shop;
    });
  }

  selectedShopConfirm()
  {
    this.selectedShop = this.selectedShopLocal;
    this.selectedShopChange.emit(this.selectedShop);
  }

  setReceiptInShopsForMap(receiptInShops: ReceiptInShopDTO[]) {
    setReceiptInShops(receiptInShops, this);
  }

}
