import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopsMapWindowComponent } from './shops-map-window.component';

describe('ShopsMapWindowComponent', () => {
  let component: ShopsMapWindowComponent;
  let fixture: ComponentFixture<ShopsMapWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShopsMapWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopsMapWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
