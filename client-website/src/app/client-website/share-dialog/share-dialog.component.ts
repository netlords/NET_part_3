import { Component, OnInit, Input } from '@angular/core';
import { UserDTO } from '../../DTO/UserDTO';
import { UserService } from '../../services/user.service';
import { SharedReceiptDTO } from '../../DTO/SharedReceiptDTO';
import { ReceiptDTO } from '../../DTO/ReceiptDTO';
import { SharedReceiptService } from '../../services/shared-receipt.service';
import { map } from 'rxjs/operators/map';

@Component({
  selector: 'app-share-dialog',
  templateUrl: './share-dialog.component.html',
  styleUrls: ['./share-dialog.component.css']
})
export class ShareDialogComponent implements OnInit {
  private _receipt: ReceiptDTO;
  users: StateUser[];
  filteredUsers: StateUser[] = new Array<StateUser>();
  filterText: string = "";

  constructor(private userService: UserService, private sharedReceiptService: SharedReceiptService) { }

  ngOnInit() {
    this.getAllUsers();
  }

  get receipt(): ReceiptDTO {
    return this._receipt;
  }

  @Input() set receipt(_rec: ReceiptDTO) {
    this._receipt = _rec;
    if(this.receipt.Id)
      this.getUsersWithAccessToReceipt();
  }

  getAllUsers(): void {
    this.userService.getAllUsers().pipe(
      map(res => {
        let users = new Array<StateUser>();
        let i = 0;
        for (let val of res) {
          users[i] = new StateUser(val, false);
          i++;
        }
        return users;
      })
    ).subscribe(res => {
      this.users = res;
      //this.getUsersWithAccessToReceipt();
      //this.getFilteredUsers();
    });
  }

  getFilteredUsers(): void {
    if (this.filterText === "") {
      this.filteredUsers = this.users;
    }
    else {
      this.filteredUsers = this.users.filter(user => user.User.Username.toLocaleLowerCase().includes(this.filterText.toLocaleLowerCase()));
    }
  }

  checkboxChanged(event: any, user: StateUser) {
    user.Checked = !user.Checked;
    let record = new SharedReceiptDTO();
    record.UserId = user.User.Id;
    record.ReceiptId = this.receipt.Id;
    if (event.target.checked) {
      this.sharedReceiptService.addShareRecord(record).subscribe();
    }
    else {
      this.sharedReceiptService.deleteShareRecord(record).subscribe();
    }
  }

  getUsersWithAccessToReceipt() {
    if (this.users && this.receipt) {
      this.sharedReceiptService.getUsersWithAccessToReceipt(this.receipt).subscribe(res => {
        for (let user of this.users) {
          if(res.find(sr => sr.Id == user.User.Id) != undefined)
          {
            user.Checked = true;
          }
          else {
            user.Checked = false;
          }
        }
        this.getFilteredUsers();
      });
    }
  }
}

class StateUser {
  User: UserDTO;
  Checked: boolean;

  constructor(user: UserDTO, checked: boolean) {
    this.User = user;
    this.Checked = checked;
  }
}