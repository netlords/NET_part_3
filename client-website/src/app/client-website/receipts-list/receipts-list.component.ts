import { Component, OnInit } from '@angular/core';
import { ReceiptService } from '../../services/receipt.service';
import { LogInService } from '../../services/log-in.service';

import { ReceiptDTO } from '../../DTO/ReceiptDTO';
import { LoginDTO } from '../../DTO/LoginDTO';

import { Observable } from 'rxjs/observable';
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';


import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { catchError, map } from 'rxjs/operators';

import { environment } from '../../../environments/environment';
import { ErrorMessage } from '../../validators/error-message';
import { ReceiptValidator } from '../../validators/receipt-validator';
// const httpOptions = {
//   headers: new HttpHeaders({ 'Content-Type': 'application/json' },
//   withCredentials: true)
// };

declare function initMaterialInputs(); 
declare function closeModal(modalid: string);

@Component({
  selector: 'app-receipts-list',
  templateUrl: './receipts-list.component.html',
  styleUrls: ['./receipts-list.component.css']
})

export class ReceiptsListComponent implements OnInit {

  receipts: ReceiptDTO[];
  selectedReceipt: ReceiptDTO = new ReceiptDTO();
  isSelectedNew: boolean = true;

  receiptValidator: ReceiptValidator = new ReceiptValidator();

  constructor(private receiptService: ReceiptService, private http: HttpClient) { }

  ngOnInit() {
    this.getReceipts();
    initMaterialInputs();
  }

  getReceipts(): void {
    this.receiptService.getReceipts().subscribe(val => {this.receipts = val});
  }

  onChangeDoneState(receipt: ReceiptDTO)
  {
    receipt.Done = ! receipt.Done;
    this.receiptService.updateReceipt(receipt).subscribe();
  }

  selectReceipt(receipt: ReceiptDTO)
  {
    this.selectedReceipt = receipt.clone();
    this.isSelectedNew = false;
    this.receiptValidator.clear();
  }

  onAddReceipt()
  {
    this.selectedReceipt = new ReceiptDTO();
    this.isSelectedNew = true;
    this.receiptValidator.clear();
  }

  onReceiptModalSave()
  {
    if(!this.receiptValidator.validate(this.selectedReceipt))
      return;
    if(this.isSelectedNew)
    {
      this.receiptService.addReceipt(this.selectedReceipt).subscribe(() => this.getReceipts());
    }
    else
    {
      this.receiptService.updateReceipt(this.selectedReceipt).subscribe(() => this.getReceipts());
    }
    closeModal("#receiptModal");
  }

  onDeleteReceipt(receipt: ReceiptDTO)
  {
    this.receiptService.deleteReceipt(receipt).subscribe(() => this.getReceipts());
  }
}
