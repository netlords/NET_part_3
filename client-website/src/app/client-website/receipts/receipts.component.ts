import { Component, OnInit, ViewChild } from '@angular/core';
import { ReceiptService } from '../../services/receipt.service';
import { ProductService } from '../../services/product.service';
import { ReceiptProductService } from '../../services/receipt-product.service';
import { ReceiptDTO } from '../../DTO/ReceiptDTO';
import { ProductDTO } from '../../DTO/ProductDTO';
import { ReceiptInShopDTO } from '../../DTO/ReceiptInShopDTO';
import { ReceiptProductDTO } from '../../DTO/ReceiptProductDTO';
import { ProductFromListInShopDTO } from '../../DTO/ProductFromListInShopDTO';
import { Observable } from 'rxjs/observable';
import { ActivatedRoute } from '@angular/router';
import { ShopDTO } from '../../DTO/ShopDTO';
import { ShopsMapWindowComponent } from '../shops-map-window/shops-map-window.component';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';

declare function addModalListener(modalId: string): any;
declare function destroyModalListener(modalId: string): any;
declare function createPdf(receipt: ReceiptDTO, products: ProductFromListInShopDTO[], shop: ShopDTO, totalPrice: number, print: boolean): any;

@Component({
  selector: 'app-receipts',
  templateUrl: './receipts.component.html',
  styleUrls: ['./receipts.component.css']
})
export class ReceiptsComponent implements OnInit, OnDestroy {
  @ViewChild(ShopsMapWindowComponent) mapComponent: ShopsMapWindowComponent;

  receipt: ReceiptDTO = new ReceiptDTO();
  products: ProductDTO[];
  filteredProducts: ProductDTO[];
  wantedProducts: ProductFromListInShopDTO[];
  selectedShop: ShopDTO;
  receiptInShops: ReceiptInShopDTO[];
  totalPrice: number;

  filterText: string = "";
  
  constructor(
    private receiptService: ReceiptService, 
    private productService: ProductService, 
    private receiptProductService: ReceiptProductService, 
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    addModalListener("#mapModal"); //in client-map.js
    this.getReceipt();
    this.getProducts();
  }

  ngOnDestroy() {
    destroyModalListener("#mapModal");
  }

  calculateTotalPrice(): void {
    let total: number = 0;
    for(let product of this.wantedProducts)
    {
      total += product.WantedQuantity * product.Product.Price;
    }
    this.totalPrice = total;
  }

  getReceipt(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.receiptService.getReceipt(id).subscribe(val => {
      this.receipt = val;
      this.getProductsFromListInShop();
      this.getReceiptInShops();
    });
  }

  getProducts(): void {
    this.productService.getProducts().subscribe(res => {
      this.products = res;
      this.getFilteredProductsNotOnList();
    });
  }

  getFilteredProductsNotOnList(): void {
    if(!this.products || !this.wantedProducts)
      return;
    if(this.filterText === "")
    {
      this.filteredProducts = this.products;
    }
    else
    {
      this.filteredProducts = this.products.filter(product => product.Name.toLocaleLowerCase().includes(this.filterText.toLocaleLowerCase()));
    }
    
    this.filteredProducts = this.filteredProducts.filter(product => !this.wantedProducts.some(wantedProduct => wantedProduct.Product.Id == product.Id));
  }
  
  getProductsFromListInShop(): void {
    this.receiptService.GetProductsFromListInShop(this.receipt.Id, this.selectedShop ? this.selectedShop.Id : -1).subscribe(val => {
      this.wantedProducts = val;
      this.getFilteredProductsNotOnList();
      this.calculateTotalPrice();
    });
  }

  getReceiptInShops(): void {
    this.receiptService.getReceiptInShops(this.receipt.Id).subscribe(res => {
      this.receiptInShops = res;
      this.mapComponent.setReceiptInShopsForMap(res);
    });
  }

  listChanged(): void {
    this.getProductsFromListInShop();
    this.getReceiptInShops();
    this.calculateTotalPrice();
  }

  addProductToReceipt(product: ProductDTO): void {
    this.receiptProductService.addProductToReceipt(ReceiptProductDTO.generate(this.receipt.Id, product.Id)).subscribe(res => {
      this.listChanged();
    });
  }

  deleteProductFromReceipt(product: ProductFromListInShopDTO): void {
    this.receiptProductService.deleteProductFromReceipt(product.ReceiptProduct).subscribe(res => {
      this.listChanged();
    });
  }

  quantityChanged(product: ProductFromListInShopDTO): void {
    this.receiptProductService.updateProductOnReceipt(product.ReceiptProduct).subscribe(res => {
      this.listChanged();      
    });
  }

  selectedShopChanged(): void {
    this.getProductsFromListInShop();
  }

  shopSelected(): void {
    this.mapComponent.selectedShopConfirm();
  }

  createPdf(print: boolean): void {
    createPdf(this.receipt, this.wantedProducts, this.selectedShop, this.totalPrice, print)
  }
}
