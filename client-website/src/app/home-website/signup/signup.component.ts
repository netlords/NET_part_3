import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RegisterDTO } from '../../DTO/RegisterDTO';
import { SignUpService } from '../../services/sign-up.service';
import { ErrorMessage } from '../../validators/error-message';

declare function showSnackbar(text);

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  signupErrorMessage: ErrorMessage = new ErrorMessage();

  constructor(private router: Router) { }

  ngOnInit() {
  }

  signUp(event: any)
  {
    event.subscribe(res => {
      if(res)
      {
        showSnackbar("Account created.");
        this.router.navigate(['/']);
      }
      else
      {
        this.signupErrorMessage.show("Sorry, sign up failed.");
      }
    })
  }
}
