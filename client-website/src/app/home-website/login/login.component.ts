import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LogInService } from '../../services/log-in.service';
import { HttpClient } from '@angular/common/http';
import { LoginDTO } from '../../DTO/LoginDTO';
import { UserService } from '../../services/user.service';
import * as Cookies from 'es-cookie';
import { ErrorMessage } from '../../validators/error-message';
import { LoginValidator } from '../../validators/login-validator';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginData: LoginDTO = new LoginDTO();
  loginErrorMessage: ErrorMessage = new ErrorMessage();
  loginValidator: LoginValidator = new LoginValidator(this.logInService);

  constructor(private logInService: LogInService, private http: HttpClient, private router: Router, private userService: UserService) { }

  ngOnInit() {
  }

  logIn()
  {
    if(!this.loginValidator.validate(this.loginData))
      return;
    this.logInService.logIn(this.loginData).subscribe(res => {
      if(res)
      {
        //this.router.navigate(['/receipts']);
        this.userService.saveCurrentUser().subscribe(res => {
          //this.router.navigate(['/seller']);        
          this.userService.redirectToHomeForUserType();
        });
      }
      else
      {
        this.loginErrorMessage.show("You entered wrong login or password.");
      }
    });
  }

  //loginWithDefaultLogin is a test function.
  //Remove when onLogIn will have similar functionality.
  //You may have to register this user first.
  // loginWithDefaultLogin(): void {
  //   let login = new LoginDTO();
  //   login.Login = 'abcdef';
  //   login.Password = 'abc';
  //   login.RememberMe = true;

  //   this.logInService.logIn(login);
  // }
  // onLogIn(loginData: LoginDTO): void {
  //   this.logInService.logIn(loginData);
  // }
}
