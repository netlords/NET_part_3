import { Component, OnInit } from '@angular/core';

declare function makeElementFullScreen(id: string): any;

@Component({
  selector: 'app-client-start-page',
  templateUrl: './client-start-page.component.html',
  styleUrls: ['./client-start-page.component.css']
})
export class ClientStartPageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    makeElementFullScreen("login-container");
  }
}
