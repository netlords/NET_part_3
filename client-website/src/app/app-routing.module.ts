import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ReceiptsListComponent } from './client-website/receipts-list/receipts-list.component';
import { ReceiptsComponent } from './client-website/receipts/receipts.component';
import { ClientStartPageComponent } from './home-website/client-start-page/client-start-page.component';
import { ShopsMapWindowComponent } from './client-website/shops-map-window/shops-map-window.component';
import { SignupComponent } from './home-website/signup/signup.component';
import { LoginComponent } from './home-website/login/login.component';
import { SellerStartPageComponent } from './seller-website/seller-start-page/seller-start-page.component';
import { ProductsListComponent } from './seller-website/products-list/products-list.component';
import { ShopsListComponent } from './seller-website/shops-list/shops-list.component';
import { ShopComponent } from './seller-website/shop/shop.component';
import { SharedReceiptsListComponent } from './client-website/shared-receipts-list/shared-receipts-list.component';
import { SharedReceiptComponent } from './client-website/shared-receipt/shared-receipt.component';
import { RegisterComponent } from './seller-website/register/register.component';

const routes: Routes = [
  //home
  { path: '', component: LoginComponent },
  { path: 'signup', component: SignupComponent },

  //client
  { path: 'receipts', component: ReceiptsListComponent },
  { path: 'receipt/:id', component: ReceiptsComponent },
  { path: 'sharedReceipts', component: SharedReceiptsListComponent },
  { path: 'sharedReceipt/:id', component: SharedReceiptComponent },

  //seller
  { path: 'seller', component: SellerStartPageComponent },
  { path: 'seller/products', component: ProductsListComponent },
  { path: 'seller/shops', component: ShopsListComponent },
  { path: 'seller/shop/:id', component: ShopComponent},
  { path: 'seller/register', component: RegisterComponent},
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}