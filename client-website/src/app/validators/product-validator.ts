import { ErrorMessage } from "./error-message";
import { ProductDTO } from "../DTO/ProductDTO";
import { ProductService } from '../services/product.service';
import { Observable } from "rxjs/Observable";
import { map } from "rxjs/operators";

export class ProductValidator {

    Price: ErrorMessage = new ErrorMessage();
    Unit: ErrorMessage = new ErrorMessage();
    Name: ErrorMessage = new ErrorMessage();

    constructor(private productService: ProductService) {
      }
    validate(product: ProductDTO, validName: string = null): Observable<boolean> {
        this.clear();
        let ret = true;
        if (!product.Price){
            this.Price.show("Price is required.");
            ret = false;
        }
        if (product.Price <= 0) {
            this.Price.show("You're selling it too cheap.");
            ret = false;
        }
        if (!product.Unit) {
            this.Unit.show("Unit is required.");
            ret = false;
        }
        if (!product.Name || product.Name == "") {
            this.Name.show("Name is required.");
            ret = false;
        }
        return this.productService.productNameAlreadyExists(product.Name, validName).pipe(
            map(res => {
                if(res)
                {
                    this.Name.show("Name already exists.");
                    ret = false;
                }
                return ret;
            })
        );
    }
    clear() {
        this.Price.IsError = false;
        this.Unit.IsError = false;
        this.Name.IsError = false;
    }
}