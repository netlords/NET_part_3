import { ErrorMessage } from "./error-message";
import { ShopDTO } from "../DTO/ShopDTO";
import { ShopService } from '../services/shop.service';
import { Observable } from "rxjs/Observable";
import { map } from "rxjs/operators";

export class ShopValidator {

    // Latitude: ErrorMessage = new ErrorMessage();
    // Longitude: ErrorMessage = new ErrorMessage();
    Localization: ErrorMessage = new ErrorMessage();
    Address: ErrorMessage = new ErrorMessage();
    Name: ErrorMessage = new ErrorMessage();

    constructor(private shopService: ShopService) {
    }
    validate(shop: ShopDTO, validName: string = null): Observable<boolean> {
        this.clear();
        let ret = true;
        if (!shop.Latitude || !shop.Longitude) {
            this.Localization.show("Localization is required.");
            ret = false;
        }
        if (shop.Latitude < -90 || shop.Latitude > 90 || shop.Longitude < -180 || shop.Longitude > 180) {
            this.Localization.show("Localization is outside of this world.");
            ret = false;
        }
        // if (!shop.Latitude){
        //     this.Latitude.show("Latitude is required.");
        //     ret = false;
        // }
        // if (shop.Latitude < -90 || shop.Latitude > 90) {
        //     this.Latitude.show("Latitude is outside of this world.");
        //     ret = false;
        // }
        // if (!shop.Longitude){
        //     this.Longitude.show("Longitude is required.");
        //     ret = false;
        // }
        // if (shop.Longitude < -180 || shop.Longitude > 180) {
        //     this.Longitude.show("Longitude is outside of this world.");
        //     ret = false;
        // }
        if (!shop.Address || shop.Address == "") {
            this.Address.show("Address is required.");
            ret = false;
        }
        if (!shop.Name || shop.Name == "") {
            this.Name.show("Name is required.");
            ret = false;
        }
        return this.shopService.shopNameAlreadyExists(shop.Name, validName).pipe(
            map(res => {
                if (res) {
                    this.Name.show("Name already exists.");
                    ret = false;
                }
                return ret;
            })
        );
    }
    clear() {
        // this.Latitude.IsError = false;
        // this.Longitude.IsError = false;
        this.Address.IsError = false;
        this.Name.IsError = false;
        this.Localization.IsError = false;
    }
}