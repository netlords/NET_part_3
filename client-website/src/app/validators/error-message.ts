export class ErrorMessage {
    Message: string;
    IsError: boolean = false;

    show(message: string) {
        this.Message = message;
        this.IsError = true;
    }
}