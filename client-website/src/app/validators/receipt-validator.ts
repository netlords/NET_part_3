import { ErrorMessage } from "./error-message";
import { ReceiptDTO } from "../DTO/ReceiptDTO";
import { ReceiptService } from '../services/receipt.service';

export class ReceiptValidator {
    Name: ErrorMessage = new ErrorMessage();

    constructor() {
      }
    validate(receipt: ReceiptDTO): boolean {
        this.clear();
        let ret = true;
        if (!receipt.Name || receipt.Name == "") {
            this.Name.show("Name is required.");
            ret = false;
        }
        return ret;
    }
    clear() {
        this.Name.IsError = false;
    }
}