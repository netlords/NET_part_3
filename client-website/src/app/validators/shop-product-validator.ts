import { ErrorMessage } from "./error-message";
import { ShopProductDTO } from "../DTO/ShopProductDTO";
import { ShopProductService } from '../services/shop-product.service';

export class ShopProductValidator {

    Quantity: ErrorMessage = new ErrorMessage();
    Aisle: ErrorMessage = new ErrorMessage();

    constructor() {
      }
    validate(shopProduct: ShopProductDTO): boolean {
        this.clear();
        let ret = true;
        if (!shopProduct.AvailableUnits){
            this.Quantity.show("Quantity is required.");
            ret = false;
        }
        if (shopProduct.AvailableUnits < 0) {
            this.Quantity.show("Quantity can't be negative.");
            ret = false;
        }
        if (!shopProduct.Aisle || shopProduct.Aisle == "") {
            this.Aisle.show("Aisle is required.");
            ret = false;
        }
        return ret;
    }
    clear() {
        this.Quantity.IsError = false;
        this.Aisle.IsError = false;
    }
}