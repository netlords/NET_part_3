import { ErrorMessage } from "./error-message";
import { LoginDTO } from "../DTO/LoginDTO";
import { LogInService } from "../services/log-in.service";

export class LoginValidator {
    Login: ErrorMessage = new ErrorMessage();
    Password: ErrorMessage = new ErrorMessage();

    constructor(private loginService: LogInService) {
    }
    validate(login: LoginDTO): boolean {
        this.clear();
        let ret = true;
        if (!login.Login || login.Login == "") {
            this.Login.show("Login is required.");
            ret = false;
        }
        if (!login.Password || login.Password == "") {
            this.Password.show("Password is required.");
            ret = false;
        }
        return ret;
    }
    clear() {
        this.Login.IsError = false;
        this.Password.IsError = false;
    }
}