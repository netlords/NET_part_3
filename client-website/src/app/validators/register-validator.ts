import { ErrorMessage } from "./error-message";
import { RegisterDTO } from "../DTO/RegisterDTO";
import { Observable } from "rxjs/Observable";
import { SignUpService } from "../services/sign-up.service";
import { map } from "rxjs/operators";

export class RegisterValidator {
    Login: ErrorMessage = new ErrorMessage();
    Password: ErrorMessage = new ErrorMessage();
    ConfirmPassword: ErrorMessage = new ErrorMessage();

    constructor(private signupService: SignUpService) { }

    validate(user: RegisterDTO): Observable<boolean> {
        this.clear();
        let ret = true;
        if (!user.Login || user.Login == "") {
            this.Login.show("Login is required.");
            ret = false;
        }
        if (!user.Password || user.Password == "") {
            this.Password.show("Password is required.");
            ret = false;
        }
        if (!user.ConfirmPassword || user.ConfirmPassword != user.Password) {
            this.ConfirmPassword.show("Passwords must match.");
            ret = false;
        }
        return this.signupService.checkIfUserNameAvailable(user.Login).pipe(
            map(res => {
                if (!res) {
                    this.Login.show("Name already exists.");
                    ret = false;
                }
                return ret;
            })
        );
    }
    clear() {
        this.Login.IsError = false;
        this.Password.IsError = false;
        this.ConfirmPassword.IsError = false;
    }
}