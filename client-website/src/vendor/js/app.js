var map;
var warszawa = {lat: 52.231593, lng: 21.033036 };
var zoomVal = 10;
var infowindow;
var sellerMarker;
var markers = [];
var markersDescs = [];
var markersShopIds = [];
var markersDescIndex = 0;
var receiptInShops, component, shop;
function initializeMap()
{
    infowindow = new google.maps.InfoWindow();
    var options = {
        zoom: zoomVal,
        center: warszawa
    };
    map = new google.maps.Map(document.getElementById("map_canvas"), options);
}

function initializeSellerMap()
{
    infowindow = new google.maps.InfoWindow();
    var options = {
        zoom: zoomVal,
        center: warszawa
    };
    map = new google.maps.Map(document.getElementById("map_canvas"), options);
    google.maps.event.addListener(map, 'click', function (event) {
        addSellerMarker(event.latLng.lat(), event.latLng.lng());
        component.setShopLocation(sellerMarker.position.lat(), sellerMarker.position.lng());
    });
}

function preapareMap()
{
    map.setCenter(warszawa);
    // map.setZoom(zoomVal);
    component.updateSelectedShopLocalSelected();
    addMarkers();
}

function addSellerMarker(lat, lng)
{
    var latLng = { lat: lat, lng: lng };
    if (sellerMarker)
        sellerMarker.setPosition(latLng);
    else {
        sellerMarker = new google.maps.Marker({
            position: latLng,
            draggable: true,
            map: map,
            icon: "assets/img/marker-green.png"
        });
    }
}

function clearSellerMarker()
{
    if(sellerMarker)
    {
        sellerMarker.setMap(null);
        sellerMarker = null;
    }
}

function addMarker(shopid, lat, lng, name, address, availability) {
    var icon = "assets/img/marker-red.png";
    if (availability > 0.4)
        icon = "assets/img/marker-yellow.png";
    if (availability > 0.8)
        icon = "assets/img/marker-green.png";
    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat, lng),
        map: map,
        icon: icon
    });
    var percent = parseInt(availability * 100);
    markers[markersDescIndex] = marker;
    markersDescs[markersDescIndex] = "<h1>" + name + "</h1>Address: <b>" + address + "</b><br>Availability: <b>" + percent + "%</b>";
    markersShopIds[markersDescIndex] = shopid;
    google.maps.event.addListener(marker, "click", (function (marker, index) {
        return function (e) {
            infowindow.setContent(markersDescs[index]);
            infowindow.open(map, marker);
            component.selectShop(markersShopIds[index]);
        }
    })(marker, markersDescIndex));
    markersDescIndex++;
}

function clearMarkers()
{
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(null);
    }

    markers = [];
    markersDescs = [];
    markersShopIds = [];
    markersDescIndex = 0;
}

function addMarkers()
{
    clearMarkers();

    receiptInShops.forEach(function(receiptInShop) {
      addMarker(receiptInShop.Shop.Id, receiptInShop.Shop.Latitude, receiptInShop.Shop.Longitude, receiptInShop.Shop.Name, receiptInShop.Shop.Address, receiptInShop.Availability);
    });
}

function setReceiptInShops(receipt, comp)
{
    receiptInShops = receipt;
    component = comp;
}

function setShop(_shop)
{
    shop = _shop;
}

function setComponent(comp)
{
    component = comp;
}

var mapInited = false;
var sellerMapInited = false;

function addModalListener(modalId)
{
    $(modalId).on('shown.bs.modal', function (e) {
        if(!mapInited)
        {
            initializeMap();
            preapareMap();
            mapInited = true;

            $(modalId).on('show.bs.modal', function (e) {
                preapareMap();
            });
        }
    });
};

function addSellerModalListener()
{
    $('#shopModal').on('shown.bs.modal', function () {
        if(!sellerMapInited)
        {
            initializeSellerMap();
            prepareSellerMap();
            sellerMapInited = true;

            $('#shopModal').on('show.bs.modal', function() {
                prepareSellerMap();
            });
        }
    });
}

function destroyModalListener(modalId, client = true)
{
    if(client)
        mapInited = false;
    else
        sellerMapInited = false;
    $(modalId).unbind('shown.bs.modal');
    $(modalId).unbind('show.bs.modal');
}


function prepareSellerMap()
{
    component.setShopInMap();
    clearSellerMarker();
    if(shop && shop.Latitude && shop.Longitude)
        addSellerMarker(shop.Latitude, shop.Longitude);
    map.setZoom(zoomVal);
    map.setCenter(warszawa);
}

//full screen components
function onResize(elementId)
{
    var height = window.innerHeight;
    $("#"+elementId).height(height + "px");
}
function makeElementFullScreen(elementId)
{
    onResize(elementId);
    $(window).on('resize', function() { onResize(elementId); });
}

//pdf
function createPdf(receipt, products, shop, totalPrice, print)
{
    var content = [];
    content.push({text: 'Get to the shopper', style: 'rightHeader'});
    content.push({text: 'List: '+receipt.Name, style: 'subheader'});
    if(shop)
        content.push({text: 'Shop: '+shop.Name, style: 'subheader'});
    var tableBody = [];
    if(shop)
    {
        tableBody.push([{text: 'Name', style: 'tableHeader'}, {text: 'Quantity', style: 'tableHeader'}, {text: 'Unit', style: 'tableHeader'}, {text: 'Aisle', style: 'tableHeader'}, {text: 'Price for unit', style: 'tableHeader'}, {text: 'Availability', style: 'tableHeader'}, {text: 'Checkmark', style: 'tableHeader'}]);
        for(var i=0; i<products.length; i++)
        {
            var product = products[i];
            tableBody.push([product.Product.Name, product.WantedQuantity, product.Product.Unit.Name(), product.ShopProduct ? product.ShopProduct.Aisle : '', Number(product.Product.Price).toFixed(2) + " PLN", (product.ShopProduct ? (product.ShopProduct.AvailableUnits > product.WantedQuantity ? {text: 'Available', style: 'success'} : { text: 'Unavailable', style: 'danger' }) : { text: 'Out of sell', style: 'danger'}), '']);
        }
    }
    else
    {
        tableBody.push([{text: 'Name', style: 'tableHeader'}, {text: 'Quantity', style: 'tableHeader'}, {text: 'Unit', style: 'tableHeader'}, {text: 'Price for unit', style: 'tableHeader'}, {text: 'Checkmark', style: 'tableHeader'}]);
        for(var i=0; i<products.length; i++)
        {
            var product = products[i];
            tableBody.push([product.Product.Name, product.WantedQuantity, product.Product.Unit.Name(), Number(product.Product.Price).toFixed(2) + " PLN", '']);
        }
    }

    content.push({
        style: 'table', table: {
            headerRows: 1,
            body: tableBody
        },
        layout: {
            fillColor: function (i, node) {
                return (i % 2 === 0) ? '#CCCCCC' : null;
            }   
        }
    });

    var docDefinition = { content: content,
    styles: {
        rightHeader: {
            fontSize: 18,
            bold: true,
            alignment: 'right'
        },
        subheader: {
            fontSize: 15,
            bold: true
        },
        tableExample: {
			margin: [0, 5, 0, 15],
			padding: [0, 5, 0, 15]
		},
		tableHeader: {
			bold: true,
			fontSize: 13,
			color: 'black'
        },
        success: {
			bold: true,
            color: 'green'
        },
        danger: {
			bold: true,
            color: 'red'
        }
    }
    };

    var pdf = pdfMake.createPdf(docDefinition)
    if(print)
        pdf.print();
    else
        pdf.download("shoppinglist.pdf");
}

//snackbar
function showSnackbar(text)
{
    $.snackbar({content: text});
}


//material input
function initMaterialInputs()
{
    $('.modal').on('show.bs.modal', function() {
        $(".form-group input").focusin()
    });

    $('input.decimal').on('focusout', function() {
        $(this).val(parseFloat($(this).val()).toFixed(2));
    })
}

//modal
function closeModal(modalid)
{
    $(modalid).modal('hide');
}